(function ($) {

Drupal.behaviors.variable_client = {
  attach: function (context, settings) {
    // Only add the button if a link is provided.
    if (typeof Drupal.settings.variable_client != "undefined") {
      var popup_link = settings.variable_client.popup_link;
      $('body').prepend(popup_link);
    }
  }
};

})(jQuery);