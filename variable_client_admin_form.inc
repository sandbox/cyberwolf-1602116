<?php
/**
 * @file
 * Form declaration, validation & submit functions for the variable admin form.
 */

 /**
  * Declares the variable admin form.
  */
function variable_client_admin_form() {
  $form['variable_search'] = array(
    '#title' => t('Search for '),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#autocomplete_path' => 'admin/content/variable-client/autocomplete',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Change this variable'),
  );

  return $form;
}

/**
 * Validation for the variable admin form.
 */
function variable_client_admin_form_validate($form, &$form_state) {
  if (!variable_client_variable_exists($form_state['values']['variable_search']) || !variable_access($form_state['values']['variable_search'])) {
    form_set_error('variable_search', t('Please use a valid, allowed variable name.'));
  }
}

/**
 * Submit handler for the variable admin form.
 */
function variable_client_admin_form_submit($form, &$form_state) {
  if (isset($_GET['page']) && strlen($_GET['page'])) {
    $form_state['redirect'] = url('admin/content/variable-client/variable/' . $form_state['values']['variable_search'], array('absolute' => TRUE)) . '?destination=' . $_GET['page'];
  }
  else {
    $form_state['redirect'] = url('admin/content/variable-client/variable/' . $form_state['values']['variable_search'], array('absolute' => TRUE)) . '?destination=admin/content/variable-client';
  }
}
