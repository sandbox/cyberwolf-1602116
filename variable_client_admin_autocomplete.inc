<?php
/**
 * @file
 * Provide autocomplete functions.
 */

/**
 * Page callback for the variable client manage admin page.
 */
function variable_client_admin_autocomplete($search) {
  $variables = variable_client_get_all_allowed_variables();
  $vset = array();

  foreach ($variables as $variable) {
    foreach ($variable as $k => $v) {
      if (preg_match("/(.{0,25}" . $search . ".{0,25})/i", $v, $parts)) {
        $base_text = $parts[1];

        $prefix = "";
        if (strpos($v, $base_text) > 0) {
          $prefix = "... ";
        }

        $suffix = "";
        if (strpos($v, $base_text) + strlen($base_text) < strlen($v)) {
          $suffix = " ...";
        }

        preg_match("/(" . $search . ")/i", $base_text, $rawparts);
        $base_text = preg_replace("/" . $rawparts[1] . "/i", "<b>" . $rawparts[1] . "</b>", $base_text);
        $vset[$variable['name']] = $variable['title'] . " - [" . $k . "] " . $prefix . $base_text . $suffix . "";
        break;
      }
    }
  }

  drupal_json_output($vset);
}
